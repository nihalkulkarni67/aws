import { INTEGER, TEXT } from 'sequelize';
import { define } from './sequelize';

const Images = define("images",{
    id:{
        type : INTEGER,
        autoIncrement : true,
        allowNull : false,
        primaryKey : true,
    },
    image :{
        type : TEXT
    }
})



export default Images;
