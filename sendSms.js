import AWS from 'aws-sdk';//npm install aws-sdk

export default function sendSms(){
    var mobileNo = '+918618448774';
    var OTP = 4516;
    
    var params = {
    Message: 'Welcome! your mobile verification code is: ' + OTP +'     Mobile Number is:' +mobileNo, /* required */
      PhoneNumber: mobileNo,
      MessageAttributes:{
          "AWS.SNS.SMS.SMSType":{
              DataType:"String",
              StringValue:"Transactional"
          },
          "AWS.SNS.SMS.SenderID":{
            DataType:"String",
            StringValue:"Horoscope"
          }
      }
      };
     let sms=  new AWS.SNS({apiVersion: 'latest'})
      sms.setSMSAttributes({
        attributes:{
            DefaultSenderID:"Horoscope",
            DefaultSMSType:"Transactional"
          }
      }) 
      return sms.publish(params).promise() 
    }