// import * as dotenv from 'dotenv'
import express from 'express';
// import mongoose from 'mongoose';
import pkg from 'body-parser';
const { urlencoded, json } = pkg;
import cors from 'cors';
// import routes from './routes';
import path from 'path';
import  run  from './aws.js';
import download from './awsPresigned.js';
import sendSms from './sendsms.js';
import sendTsms from './sendTsms.js';
import dotenv from 'dotenv';

dotenv.config();//npm install dotenv

const app = express();
app.use(urlencoded({ extended: false }));
app.use(json());
app.use(cors());

// startDatabase();
app.put('/uploadlocal',
(req,res)=>{
    console.log(req.body);
    let data =  run.run();
    res.send(data);
}
)
app.get('/downloadlocal',async (req,res)=>{     // req object should look like this {"fname":"r1.png"}
    let data =await download(req.body.fname);
    res.json({data:data});
})

app.post('/sendsms',(req,res)=>{
    sendSms().then(data=>{
        console.log(data);
        res.send(data);
    }).catch(err=>res.json({status:"err",
err:err}));
});

app.post('/sendtsms',(req,res)=>{
    sendTsms().then(data=>{
        console.log(data);
        res.send(data);
    }).catch(err=>res.json({status:"err",
err:err}));
});
app.listen(8235, () => console.log('nihal aws Server is listening on port',8235));